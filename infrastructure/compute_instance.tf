module "instance" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/0.1.3.zip"

  providers = {
    openstack = openstack
  }

  app_name                = "registry"
  instance_public_ssh_key = var.INSTANCE_PUBLIC_SSH_KEY
}

resource "null_resource" "instance_add_ip_to_hosts" {
  provisioner "local-exec" {
    command = "ssh-keyscan ${module.instance.floating_ip} >> ~/.ssh/known_hosts"
  }
}
