resource "local_sensitive_file" "tls_proxy_tls_cert" {
  filename = "${local.tls_certs_dir}/cert.crt"
  content  = "${acme_certificate.app_subdomain.certificate_pem}${acme_certificate.app_subdomain.issuer_pem}"

  file_permission = "700"
}

resource "local_sensitive_file" "tls_proxy_tls_cert_key" {
  filename = "${local.tls_certs_dir}/cert.key"
  content  = acme_certificate.app_subdomain.private_key_pem

  file_permission = "700"
}
