data "exoscale_domain" "root" {
  name = var.APP_DOMAIN
}

resource "exoscale_domain_record" "app_subdomain" {
  domain      = data.exoscale_domain.root.id
  name        = "registry"
  record_type = "A"
  content     = module.instance.floating_ip
}
