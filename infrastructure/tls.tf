resource "tls_private_key" "app_subdomain" {
  algorithm = "RSA"
}

resource "acme_registration" "app_subdomain" {
  account_key_pem = tls_private_key.app_subdomain.private_key_pem
  email_address   = var.ACME_REGISTRATION_EMAIL
}

resource "acme_certificate" "app_subdomain" {
  account_key_pem = acme_registration.app_subdomain.account_key_pem
  common_name     = "${exoscale_domain_record.app_subdomain.name}.${data.exoscale_domain.root.name}"

  dns_challenge {
    provider = "exoscale"

    config = {
      EXOSCALE_API_KEY    = var.EXOSCALE_API_KEY
      EXOSCALE_API_SECRET = var.EXOSCALE_API_SECRET
    }
  }
}
