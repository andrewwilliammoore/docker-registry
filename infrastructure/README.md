Infrastructure Configuration
============

Uses Terraform
---------------------------------------------

## Getting Started
CD into this directory
```bash
cd infrastructure
```

Create a gitignored file `.envrc` with the following:
```bash
#!/usr/bin/env bash

# Openstack
# These values can be found from your openstack provider, such as from a downloadable openrc.sh file.

# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
export OS_AUTH_URL=OS_AUTH_URL

# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_APPLICATION_CREDENTIAL_ID=OS_APPLICATION_CREDENTIAL_ID

# With Keystone you pass the keystone application_credential_secret.
export OS_APPLICATION_CREDENTIAL_SECRET=OS_APPLICATION_CREDENTIAL_SECRET

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=OS_REGION_NAME
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi

export OS_AUTH_TYPE="v3applicationcredential"
export OS_IDENTITY_API_VERSION=3

# Exoscale

export EXOSCALE_API_KEY=
export EXOSCALE_API_SECRET=

# Terraform

export TF_VAR_APP_DOMAIN=...; #myapp.com etc.

# For HTTP Proxy
export TF_VAR_INSTANCE_PUBLIC_SSH_KEY=""; # Use ssh-keygen on host
export TF_VAR_ACME_REGISTRATION_EMAIL="";
export TF_VAR_EXOSCALE_API_KEY=$EXOSCALE_API_KEY;
export TF_VAR_EXOSCALE_API_SECRET=$EXOSCALE_API_SECRET;
```

Load environment variables
```bash
. ./.envrc
```

Create an object storage with your provider that will hold the terraform state. Name it `portfolio-backend`.

Initialize Terraform
```bash
tfenv install
tfenv use
terraform init
```

Apply Infrastructure
```bash
terraform apply
```
