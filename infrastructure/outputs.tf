output "instance_public_ip" {
  value = module.instance.floating_ip
}

output "tls_certs_full_path" {
  value = "${path.cwd}/${local.tls_certs_path}"
}

output "registry_username" {
  value = random_id.registry_username.b64_std
  sensitive = true
}

output "registry_password" {
  value = random_id.registry_password.b64_std
  sensitive = true
}
