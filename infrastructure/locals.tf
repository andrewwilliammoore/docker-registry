locals {
  tls_certs_path = "conf.d/certs"
  tls_certs_dir  = "${path.module}/${local.tls_certs_path}"
}
