resource "random_id" "registry_username" {
  byte_length = 8
}

resource "random_id" "registry_password" {
  byte_length = 8
}
