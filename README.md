# docker-registry

## Infrastructure

[./infrastructure](./infrastructure)

## Deploy Registry Service

```sh
./bin/deploy.sh
```
