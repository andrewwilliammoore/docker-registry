#!/bin/bash

set -eu

cd infrastructure
. .envrc
tfenv use

export DOCKER_HOST="ssh://ubuntu@$(terraform output instance_public_ip | tr -d \")"
export TLS_CERTS_FULL_PATH="$(terraform output tls_certs_full_path | tr -d \")"
export REGISTRY_USERNAME="$(terraform output registry_username | tr -d \")"
export REGISTRY_PASSWORD="$(terraform output registry_password | tr -d \")"

stop_existing_container () {
  container_name=$1

  if docker container ls -a | grep $container_name; then
    docker stop $container_name
    docker rm $container_name
  fi
}

registry_auth_path=/usr/local/apache2/auth/htpasswd

volume_name=auth
docker volume create $volume_name

docker run \
  --entrypoint htpasswd \
  -v $volume_name:/usr/local/apache2/auth \
  httpd:2.4.54-alpine -Bbc $registry_auth_path $REGISTRY_USERNAME $REGISTRY_PASSWORD

instance_name=registry_instance
stop_existing_container $instance_name
# Cert file names need to match infra config naming
docker run -d \
  --restart=always \
  --name $instance_name \
  -v $volume_name:/usr/local/apache2/auth \
  -e REGISTRY_AUTH=htpasswd \
  -e REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=$registry_auth_path \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/cert.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/cert.key \
  -p 443:443 \
  registry:2.8.1

docker cp $TLS_CERTS_FULL_PATH $instance_name:/certs
docker restart $instance_name
